package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import org.junit.Assert;
import utilities.RestAssuredExtension;

import java.util.HashMap;

public class PostsSteps {
    public static ResponseOptions<Response> response;

    @Given("^Perform POST operation for the url \"([^\"]*)\" ([^\"]*) and ([^\"]*)$")
    public void performPOSTOperationForTheUrlCountryBusinessChannelAndUsername(String url, String countryBusinessChannel, String username) {
        HashMap<String, String> body = new HashMap<String, String>();
        body.put("countryBusinessChannel", countryBusinessChannel);
        body.put("username", username);

        response = RestAssuredExtension.Post(url, body);
        System.out.println(body);
    }

    @When("^Perform POST Check Status Code result get ([^\"]*)$")
    public void performPOSTCheckStatusCodeResultGetCode(int code) {
        Assert.assertEquals(code, response.getStatusCode());
        System.out.println(response.getStatusCode());
    }

    @Then("^Perform POST Check Body result Internal Code ([^\"]*)$")
    public void performPOSTCheckBodyResultInternalCodeIcode(int icode) {
        Assert.assertEquals(icode, response.getBody().jsonPath().get("code"));
        System.out.println(response.getBody().jsonPath().get("code"));
    }

    @And("^Perform POST Check Body result Message ([^\"]*)$")
    public void performPOSTCheckBodyResultMessageResultMessage(String message) {
        Assert.assertEquals(message, response.getBody().jsonPath().get("message"));
        System.out.println(response.getBody().jsonPath().get("message"));
    }
}
