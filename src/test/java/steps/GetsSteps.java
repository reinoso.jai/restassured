package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import org.junit.Assert;
import utilities.RestAssuredExtension;

public class GetsSteps {
    public static ResponseOptions<Response> response;

    @Given("^Perform GET operation for the url \"([^\"]*)\"$")
    public void performGETOperationForTheUrl(String url) {
        response = RestAssuredExtension.Get(url);
    }

    @When("^Check Status Code result (\\d+)$")
    public void checkStatusCodeResult(int code) {
        Assert.assertEquals(code, response.getStatusCode());
        System.out.println(response.getStatusCode());
    }

    @Then("^Check Body result \"([^\"]*)\"$")
    public void checkBodyResult(String result) {
        Assert.assertEquals(result, response.getBody().print());
    }
}
