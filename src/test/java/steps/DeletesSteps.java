package steps;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import org.junit.Assert;
import utilities.RestAssuredExtension;

import java.util.HashMap;

public class DeletesSteps {
    public static ResponseOptions<Response> response;

    @Given("^Perform DELETE Claim Store operation for the url \"([^\"]*)\"$")
    public void performDELETEClaimStoreOperationForTheUrl(String url, DataTable table) {
        HashMap<String, String> body = new HashMap<String, String>();
        body.put("id", table.raw().get(1).get(0));
        body.put("requestedBy", table.raw().get(1).get(1));

        response = RestAssuredExtension.Delete(url, body);
    }

    @When("^DELETE Claim Store Check Status Code result (\\d+)$")
    public void deleteClaimStoreCheckStatusCodeResult(int code) {
        Assert.assertEquals(code, response.getStatusCode());
        System.out.println(response.getStatusCode());
    }

    @Then("^DELETE Claim Store Check Body result Internal code (\\d+)$")
    public void deleteClaimStoreCheckBodyResultInternalCode(int icode) {
        Assert.assertEquals(icode, response.getBody().jsonPath().get("code"));
        System.out.println(response.getBody().jsonPath().get("code"));
    }

    @And("^DELETE Claim Store Check Body result Message \"([^\"]*)\"$")
    public void deleteClaimStoreCheckBodyResultMessage(String message) {
        Assert.assertEquals(message, response.getBody().jsonPath().get("message"));
        System.out.println(response.getBody().jsonPath().get("message"));
    }
}
