package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import org.junit.Assert;
import utilities.RestAssuredExtension;

import java.util.HashMap;

public class VUClaimSteps {
    public static ResponseOptions<Response> response;

    @Given("^Perform POST SAVE Claim Store operation for the url \"([^\"]*)\", ([^\"]*) and ([^\"]*)$")
    public void performPOSTSAVEClaimStoreOperationForTheUrlNameAndRequestedBy(String url, String name, String requestedBy) {
        HashMap<String, String> body = new HashMap<String, String>();

        body.put("name", name);
        body.put("requestedBy", requestedBy);

        response = RestAssuredExtension.Post(url, body);
        System.out.println(body);
    }

    @When("^Check POST SAVE Claim Store the Status Code ([^\"]*) of the service$")
    public void checkPOSTSAVEClaimStoreTheStatusCodeCodeOfTheService(int code) {
        Assert.assertEquals(code, response.getStatusCode());
        System.out.println(response.getStatusCode());
    }

    @Then("^Check POST SAVE Claim Store the result of the body Internal code ([^\"]*) of the service$")
    public void checkPOSTSAVEClaimStoreTheResultOfTheBodyInternalCodeIcodeOfTheService(int icode) {
        Assert.assertEquals(icode, response.getBody().jsonPath().get("code"));
        System.out.println(response.getBody().jsonPath().get("code"));
    }

    @And("^Check POST SAVE Claim Store the Body result Message ([^\"]*) of the service$")
    public void checkPOSTSAVEClaimStoreTheBodyResultMessageResultMessageOfTheService(String message) {
        Assert.assertEquals(message, response.getBody().jsonPath().get("message"));
        System.out.println(response.getBody().jsonPath().get("message"));
    }

    @Given("^Perform GET GETALL Claim Store operation for the url \"([^\"]*)\"$")
    public void performGETGETALLClaimStoreOperationForTheUrl(String arg0)  {

    }

    @When("^Check GET ALL Claim Store the Status Code ([^\"]*) of the service$")
    public void checkGETALLClaimStoreTheStatusCodeCodeOfTheService() {

    }

    @Then("^Check GET ALL Claim Store the result of the body of the service$")
    public void checkGETALLClaimStoreTheResultOfTheBodyOfTheService() {
    }
}
