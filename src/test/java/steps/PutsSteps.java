package steps;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import org.junit.Assert;
import utilities.RestAssuredExtension;

import java.util.HashMap;

public class PutsSteps {
    public static ResponseOptions<Response> response;

    @Given("^Perform PUT operation for the url \"([^\"]*)\"$")
    public void performPUTOperationForTheUrl(String url, DataTable table) {
        HashMap<String, String> body = new HashMap<String, String>();
        body.put("description", table.raw().get(1).get(0));
        body.put("id", table.raw().get(1).get(1));
        body.put("name", table.raw().get(1).get(2));
        body.put("requestedBy", table.raw().get(1).get(3));

        response = RestAssuredExtension.Put(url, body);
    }

    @When("^Check Status Code result of PUT (\\d+)$")
    public void checkStatusCodeResultOfPUT(int code) {
        Assert.assertEquals(code, response.getStatusCode());
        System.out.println(response.getStatusCode());
    }

    @Then("^Check Body result Internal code of PUT (\\d+)$")
    public void checkBodyResultInternalCodeOfPUT(int icode) {
        Assert.assertEquals(icode, response.getBody().jsonPath().get("code"));
        System.out.println(response.getBody().jsonPath().get("code"));
    }

    @And("^Check Body result Message of PUT \"([^\"]*)\"$")
    public void checkBodyResultMessageOfPUT(String message) {
        Assert.assertEquals(message, response.getBody().jsonPath().get("message"));
        System.out.println(response.getBody().jsonPath().get("message"));
    }
}
