Feature:Verify different CLAIM operations
  Scenario Outline: Verify POST SAVE Claim Store endpoint service of Ola 1
    Given Perform POST SAVE Claim Store operation for the url "/claimStore/claim/save", <name> and <requestedBy>
    When Check POST SAVE Claim Store the Status Code <code> of the service
    Then Check POST SAVE Claim Store the result of the body Internal code <icode> of the service
    And Check POST SAVE Claim Store the Body result Message <resultMessage> of the service

    Examples:
      |  name  | requestedBy | code | icode | resultMessage                |
      | Gender |     rr      | 200  | 701   | Claim created successfully   |

  Scenario Outline: Verify GET GETALL Claim Store endpoint service of Ola 1
    Given Perform GET GETALL Claim Store operation for the url "/claimStore/claim/getAll"
    When Check GET ALL Claim Store the Status Code <code> of the service
    Then Check GET ALL Claim Store the result of the body of the service

    Examples:
      | code |
      | 200  |


