Feature: Verify different POST operations
  Scenario Outline: Verify Post Status Password service of Ola 0
    Given Perform POST operation for the url "/vuserver/connector/tokens/password/status.php" <countryBusinessChannel> and <username>
    When Perform POST Check Status Code result get <code>
    Then Perform POST Check Body result Internal Code <icode>
    And Perform POST Check Body result Message <resultMessage>

    Examples:
      | countryBusinessChannel | username | code | icode |     resultMessage      |
      |          CL-FAL        |  ALANAM  |  200 |  201  | Operation Successfully |
      |          CL-FAL        |    ALAN  |  400 |  106  |   Username too short   |