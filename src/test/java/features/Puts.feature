Feature: Verify different PUT operations
  Scenario: Verify Put Claim Store endpoint update service of Ola 1
    Given Perform PUT operation for the url "/claimStore/claimSet/update"
      | description | id |  name  | requestedBy |
      |   jaime2    | 17 | jaime2 |    string   |
    When Check Status Code result of PUT 200
    Then Check Body result Internal code of PUT 723
    And Check Body result Message of PUT "Claim set updated successfully"