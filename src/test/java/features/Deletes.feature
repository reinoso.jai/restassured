Feature: Verify different DELETE operations
  Scenario: Verify Put Claim Store endpoint update service of Ola 1
    Given Perform DELETE Claim Store operation for the url "/claimStore/claimSet/delete"
      | id | requestedBy |
      | 19 |    string   |
    When DELETE Claim Store Check Status Code result 200
    Then DELETE Claim Store Check Body result Internal code 725
    And DELETE Claim Store Check Body result Message "Claim set deleted successfully"