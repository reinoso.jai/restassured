Feature: Verify different GET operations
  Scenario: Verify Get Version service of Ola 0
    Given Perform GET operation for the url "/vuserver/connector/version.php"
    When Check Status Code result 200
    Then Check Body result "5.9.0.9"