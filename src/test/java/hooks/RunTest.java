package hooks;

import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;


@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/java/features",
        glue = "steps",
        plugin = {"pretty", "com.cucumber.listener.ExtentCucumberFormatter:"}
)
public class RunTest {
    @BeforeClass
    public static void setUpPath(){
        String reportPath = "src/test/java/reportes/html/";
        String reportName = "Reporte-AUT_" + new SimpleDateFormat("dd-MM-YYYY_hh-mm-ss").format(new Date()) + ".html";
        ExtentProperties extentProperties = ExtentProperties.INSTANCE;
        extentProperties.setReportPath(reportPath + reportName);
    }

    @AfterClass
    public static void setUp(){
        Reporter.loadXMLConfig(new File("src/test/java/reportes/config/extent-config.xml"));
        Reporter.setSystemInfo("Nombre del Proyecto", "Pruebas AUT");
        Reporter.setSystemInfo("Zona Horaria", System.getProperty("user.timezone"));
        Reporter.setSystemInfo("Ubicacion Ejecucion", System.getProperty("user.country"));
        Reporter.setSystemInfo("Nombre OS", System.getProperty("os.name"));
    }
}
