package utilities;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

public class RestAssuredExtension {
    public static RequestSpecification request;

    public RestAssuredExtension(){
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri("https://ciam-be-dev.falabella.com");
        builder.setContentType(ContentType.JSON);
        request = RestAssured.given().spec(builder.build());
    }

    public static ResponseOptions<Response> Get(String url) {
        try {
            return request.get(new URI(url));
        }catch (URISyntaxException e){
            e.printStackTrace();
        }
        return null;
    }

    public static ResponseOptions<Response> Post(String url, Map<String, String> body) {
        request.body(body);
        try {
            return request.post(new URI(url));
        }catch (URISyntaxException e){
            e.printStackTrace();
        }
        return null;
    }

    public static ResponseOptions<Response> Put(String url, Map<String, String> body) {
        request.body(body);
        try {
            return request.put(new URI(url));
        }catch (URISyntaxException e){
            e.printStackTrace();
        }
        return null;
    }

    public static ResponseOptions<Response> Delete(String url, Map<String, String> body) {
        request.body(body);
        try {
            return request.delete(new URI(url));
        }catch (URISyntaxException e){
            e.printStackTrace();
        }
        return null;
    }
}
